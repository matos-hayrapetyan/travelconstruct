<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('/about-us', function () {
//    return view('pages.about-us');
//});
//
//Route::get('/contact-us', function () {
//    return view('pages.contact-us');
//});
//
//Route::get('/pricing', function () {
//    return view('pages.pricing');
//});
//
//Auth::routes();
//
//Route::group(['prefix' => 'control-panel', 'middleware' => 'auth'], function () {
//    Route::get('/','ControlPanel\HomeController@index');
//});

/**
 *
 */
Route::any('{path}', function()
{
    return File::get(public_path() . '/ng-build/index.html');
})->where('path', '^(?!api).*$');