<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/token', 'Auth\DefaultController@authenticate');
Route::post('auth/register', 'Auth\DefaultController@register');

Route::middleware(['auth:api'])->group(function () {
    Route::get('/users/{user}', 'UsersController@show');

    Route::get('users/{user}/hotels', 'UserHotelsController@index');
    Route::post('users/{user}/hotels','UserHotelsController@store');
    Route::delete('users/{user}/hotels/{hotel}','UserHotelsController@destroy');

    Route::get('users/{user}/seasons', 'UserSeasonsController@index');
    Route::post('users/{user}/seasons', 'UserSeasonsController@store');
    Route::delete('users/{user}/seasons/{season}', 'UserSeasonsController@destroy');
});

Route::get('cities', 'CitiesController@index');
Route::get('countries', 'CountriesController@index');
//Route::resource('hotels', 'HotelsController');
