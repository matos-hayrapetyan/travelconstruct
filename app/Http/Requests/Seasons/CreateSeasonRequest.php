<?php

namespace App\Http\Requests\Seasons;

use Illuminate\Foundation\Http\FormRequest;

class CreateSeasonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'start_day' => 'required',
            'start_month' => 'required',
            'end_day' => 'required',
            'end_month' => 'required',
        ];
    }
}
