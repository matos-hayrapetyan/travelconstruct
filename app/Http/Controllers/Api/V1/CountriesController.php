<?php

namespace App\Http\Controllers\Api\V1;

use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CountriesController extends Controller
{

    public function __construct()
    {

    }

    /**
     * GET /countries
     */
    public function index(Request $request)
    {
        $query = Country::query();

        if(!empty($request->code_alpha3)){
            $query->where('code_alpha3','=', $request->code_alpha3);
        }

        if(!empty($request->code_alpha2)){
            $query->where('code_alpha2','=', $request->code_alpha2);
        }

        if(!empty($request->name)){
            $query->where('name','=', $request->name);
        }


        if(!empty($request->search)){
            $search = trim($request->search);
            /**
             * if the search is ALL CAPS than perform a EQUAL TO search,
             * otherwise perform a search with LIKE operator
             */
            if(strtoupper($search) === $search ){
                $operator = '=';
                if(strlen($search) === 3){
                    $colName = 'code_alpha3';
                }else{
                    $colName = 'code_alpha2';
                }
            }else{
                $search = '%'.$search.'%';
                $operator = 'LIKE';
                $colName = 'name';
            }
            $query->where($colName,$operator, $search);
        }



        return response()->json($query->get(['id','name','code_alpha2','code_alpha3','currency']));
    }

    /**
     * GET /countries/{slug}
     *
     * @param Country $country
     */
    public function show(Country $country)
    {

    }

    /**
     * GET /countries/create
     */
    public function create()
    {

    }

    /**
     * POST /countries
     */
    public function store()
    {


    }

    /**
     * GET /countries/{id}/edit
     * @param Country $country
     */
    public function edit(Country $country)
    {

    }

    /**
     * PUT/PATCH /countries/{id}
     */
    public function update()
    {

    }

    /**
     * DELETE /countries/{id}
     */
    public function destroy()
    {

    }

}