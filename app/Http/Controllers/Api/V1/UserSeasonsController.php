<?php

namespace App\Http\Controllers\Api\V1;

use App\Hotel;
use App\Http\Requests\Seasons\CreateSeasonRequest;
use App\Season;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserSeasonsController
{

    public function index(User $user)
    {
        $response = $user->seasons()
            ->select(['id','name','start_day','start_month','start_year','end_day','end_month','end_year'])
            ->orderBy('id','desc')
            ->get();

        return response()->json($response);
    }

    /**
     * POST /api/v1/users/{user}/seasons
     *
     * @param User $user
     * @param CreateSeasonRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(User $user, CreateSeasonRequest $request)
    {
        $season = new Season([
            'name' => $request->name,
            'start_day' => $request->start_day,
            'start_month' => $request->start_month,
            'start_year' => $request->start_year !== 'repeat' ? $request->start_year : null,
            'end_day' => $request->end_day,
            'end_month' => $request->end_month,
            'end_year' => $request->end_year !== 'repeat' ? $request->end_year : null,
        ]);

        $user->seasons()->save($season);

        return response()->json(['message' => 'Season Created successfully', 'id' => $season->id]);
    }

    /**
     * DELETE /api/v1/users/{user}/seasons/{id}
     * @param User $user
     * @param Season $season
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user, Season $season)
    {
        $season->user()->dissociate();
        $season->roomTypes()->detach();

        $season->delete();

        return response()->json(['message' => 'Season Removed successfully']);
    }


}