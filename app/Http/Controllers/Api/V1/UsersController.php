<?php

namespace App\Http\Controllers\Api\V1;


use App\User;

class UsersController
{

    public function __construct()
    {

    }

    /**
     * GET /users
     */
    public function index(Request $request)
    {
        return response()->json(User::select(['id','name', 'email'])->get());
    }

    /**
     * GET /users/{id}
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    {
        return response()->json($user);
    }


}