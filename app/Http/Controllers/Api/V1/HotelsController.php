<?php

namespace App\Http\Controllers\Api\V1;

use App\City;
use App\Hotel;
use App\Http\Controllers\Controller;
use App\Http\Requests\Hotels\CreateHotelRequest;
use App\RoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HotelsController extends Controller
{

    public function __construct()
    {

    }

    /**
     * GET /hotels
     */
    public function index(Request $request)
    {
        return response()->json(Hotel::select(['id','name'])->get());
    }
}