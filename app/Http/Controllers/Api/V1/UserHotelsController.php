<?php

namespace App\Http\Controllers\Api\V1;


use App\Hotel;
use App\Http\Requests\Hotels\CreateHotelRequest;
use App\RoomType;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserHotelsController extends Controller
{

    /**
     * GET /api/v1/users/{user}/hotels
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(User $user){
        $response = $user->hotels()

            ->with(['city'=> function($query){
                $query->select(['id','name','country_id']);
            }])
            ->with(['country'=> function($query){
                $query->select(['id','name','code_alpha2','code_alpha3']);
            }])
            ->with(['roomTypes'=> function($query){
                $query->select(['id','hotel_id','name','max_guests','room_count'])
                    ->with(['seasons' => function($query){
                        $query->select(['room_type_id','season_id','price','name','start_day','start_month','start_year','end_day','end_month','end_year']);
                    }]);
            }])
            ->select(['id','name','description','stars','country_id','city_id'])
            ->orderBy('id','desc')
            ->get();

        return response()->json($response);
    }

    /**
     * POST /api/v1/users/{user}/hotels
     *
     * @param User $user
     * @param CreateHotelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(User $user, CreateHotelRequest $request)
    {
        $hotel = new Hotel([
           'name' => $request->name,
            'description' => $request->description,
            'stars' => $request->stars,
            'city_id' =>$request->city_id,
            'country_id' => $request->country_id,
            'address' => $request->address
        ]);

        $user->hotels()->save($hotel);


        if(!empty($request->room_types)){

            foreach($request->room_types as $room_type){

                $newRoomType = new RoomType([
                    'name' => $room_type['name'],
                    'max_guests' => $room_type['max_guests'],
                    'room_count' => $room_type['room_count'],
                    'price' => isset($room_type['price']) ? $room_type['price'] : 0,
                    'hotel_id' => $hotel->id,
                ]);

                $hotel->roomTypes()->save($newRoomType);

                if(!empty($room_type['seasons'])){
                    $newRoomType->seasons()->attach($room_type['seasons']);
                }

            }
        }

        return response()->json(['message'=>'Hotel Created successfully', 'id'=>$hotel->id]);
    }

    /**
     * DELETE /api/v1/users/{user}/hotels/{hotel}
     * @param User $user
     * @param Hotel $hotel
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user, Hotel $hotel)
    {
        $hotel->user()->dissociate();

        $hotel->delete();

        return response()->json(['message' => 'Hotel Removed successfully']);
    }


}