<?php

namespace App\Http\Controllers\Api\V1;

use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CitiesController extends Controller
{

    public function __construct()
    {

    }

    /**
     * GET /cities
     */
    public function index(Request $request)
    {
        $query = City::query()->join('countries','countries.id','=','cities.country_id');

//        $cities = City::select('name', 'id', 'country_id')->with(['country' => function ($query) {
//            $query->select('id', 'name');
//        }])->limit(100)->get();
//
//        return response()->json($cities);

        if(empty($request->fields)){
            $query->select([
                'cities.id as id',
                'cities.name as name',
                'cities.latitude as latitude',
                'cities.longitude as longitude',
                'countries.code_alpha2 as country_code_alpha2',
                'countries.code_alpha3 as country_code_alpha3',
                'countries.name as country_name',
            ]);
        }else{
            $fields = explode(',',$request->fields);
            $select = [];

            foreach($fields as $field){
                $select[] = 'cities.'.$field.' as '.$field;
            }
            $query->select($select);
        }



        if(!empty($request->name)){
            $query->where('cities.name','=', $request->name);
        }

        if(!empty($request->country_id)){
            $query->where('cities.country_id','=', $request->country_id);
        }

        if(!empty($request->latitude)){
            $query->where('cities.latitude','=', $request->latitude);
        }

        if(!empty($request->longitude)){
            $query->where('cities.longitude','=', $request->longitude);
        }

        if(!empty($request->search)){
            $query->where('cities.name', 'LIKE', '%'.$request->search.'%');
        }

        if(!empty($request->country)){
            $country = trim($request->country);
            /**
             * if the country is ALL CAPS than perform a EQUAL TO search,
             * otherwise perform a search with LIKE operator
             */
            if(strtoupper($country) == $country ){
                $operator = '=';
                if(strlen($country) === 3){
                    $colName = 'countries.code_alpha3';
                }else{
                    $colName = 'countries.code_alpha2';
                }
            }else{
                $country = '%'.$country.'%';
                $operator = 'LIKE';
                $colName = 'countries.name';
            }
            $query->where($colName, $operator, $country);
        }

        return response()->json($query->get());
    }

    /**
     * GET /cities/{slug}
     *
     * @param City $city
     */
    public function show(City $city)
    {

    }

    /**
     * GET /cities/create
     */
    public function create()
    {

    }

    /**
     * POST /cities
     */
    public function store()
    {


    }

    /**
     * GET /cities/{id}/edit
     * @param City $city
     */
    public function edit(City $city)
    {

    }

    /**
     * PUT/PATCH /cities/{id}
     */
    public function update()
    {

    }

    /**
     * DELETE /cities/{id}
     */
    public function destroy()
    {

    }

}