<?php

namespace App\Http\Controllers\ControlPanel;

use App\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {

    }

    public function index(){
        $hotels = Hotel::where('user_id',Auth::user()->id)->get();
        return view('control-panel.index',compact('hotels'));
    }

}
