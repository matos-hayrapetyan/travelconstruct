<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name', 'code_alpha2','code_alpha3','currency'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'code_alpha2';
    }

    public function cities()
    {
        return $this->hasMany('App\City');
    }

    public function hotels()
    {
        return $this->hasMany('App\Hotel');
    }

    public function getCurrencyAttribute($value)
    {
        $unserializedValue = @unserialize($value);
        if(false !== $unserializedValue){

            return $unserializedValue;
        }

        return $value;
    }
}