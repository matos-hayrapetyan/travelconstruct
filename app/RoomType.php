<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $fillable = ['hotel_id','name','max_guests','room_count','price'];


    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    /**
     * The users that belong to the role.
     */
    public function seasons()
    {
        return $this->belongsToMany('App\Season')->using('App\SeasonRoomType');
    }
}
