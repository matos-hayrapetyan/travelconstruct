<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{

    protected $fillable = ['user_id','name', 'start_day','start_month', 'start_year', 'end_day', 'end_month', 'end_year'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function roomTypes()
    {
        return $this->belongsToMany('App\RoomType')->using('App\RoomTypeSeason');
    }

}