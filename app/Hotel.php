<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $fillable = ['user_id','city_id','country_id','address', 'name','description','stars'];

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function roomTypes()
    {
        return $this->hasMany('App\RoomType');
    }
}
