<?php

use App\Country;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = include(resource_path('data/world-cities.txt'));
        $countries = Country::all();
        $sortedCountries = [];

        foreach($countries as $country){
            $sortedCountries[$country->code_alpha2] = $country->id;
        }

        $data = [];
        $i = 0;
        foreach($cities as $city){
            if(++$i < 5000){
                $data[] = [
                    'name' => $city['city'],
                    'country_id' => $sortedCountries[$city['country']],
                    'latitude' => $city['latitude'],
                    'longitude' => $city['longitude'],
                ];
            }else{
                DB::table('cities')->insert($data);
                $data = [];
                $i = 0;
            }
        }
    }
}
