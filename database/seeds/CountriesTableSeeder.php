<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = include(resource_path('data/world-countries.php'));
        $data = [];
        foreach($countries as $country){
            $data[] = [
                'name' => $country['name'],
                'code_alpha2' => $country['alpha2'],
                'code_alpha3' => $country['alpha3'],
                'currency' => is_string($country['currency']) ? $country['currency'] : @serialize($country['currency']),
            ];

        }
        DB::table('countries')->insert($data);
    }
}
