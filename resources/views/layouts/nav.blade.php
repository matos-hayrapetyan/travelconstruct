<nav id="nav">
    <a class="brand" href="{{ url('/') }}">TravelConstruct</a>
    <ul class="nav-items nav-left">
        <li><a {{ Request::is('/') ? 'class=active' : null }} href="{{ url('/') }}">Home</a></li>
        <li><a {{ Request::is('pricing') ? 'class=active' : null }} href="{{ url('pricing') }}">Pricing</a></li>
        <li><a {{ Request::is('about-us') ? 'class=active' : null }} href="{{ url('about-us') }}">About Us</a></li>
        <li><a {{ Request::is('contact-us') ? 'class=active' : null }} href="{{ url('contact-us') }}">Contact Us</a></li>
    </ul>
    <ul class="nav-items nav-right">
        @if(Auth::guest())
            <li ><a href="{{ route('login') }}" >Login</a></li>
            <li ><a href="{{ route('register') }}" >Register</a></li>
        @else
            <li class="dropdown">
                <a href="#">Welcome, {{ Auth::user()->name }}</a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('logout') }}">Logout</a></li>
                </ul>
            </li>
        @endif
    </ul>
</nav>
