@extends('layouts.control-panel')

@section('title', 'Control Panel | TravelConstruct')

@section('content')
    <h1>welcome {{ Auth::user()->name }}</h1>
    <div class="card">
        <h2>Hotels</h2>
        <button class="button" @click="showNewHotelModal = true">New Hotel</button>
        <new-hotel-modal :show="showNewHotelModal" @close="showNewHotelModal = false" ></new-hotel-modal>
        <table>
            <tr>
                <th>Name</th>
                <th>City</th>
                <th>Stars</th>
            </tr>
           @forelse($hotels as $hotel)
                <tr>
                    <td>{{ $hotel->name }}</td>
                    <td>{{ $hotel->city }}</td>
                    <td>{{ $hotel->stars }}</td>
                </tr>
            @empty
                <tr><td colspan="3">No hotels</td></tr>
            @endforelse
        </table>

    </div>
@endsection