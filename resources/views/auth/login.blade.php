@extends('layouts.app')

@section('content')
    <login action="{{ route('login') }}" password_request="{{ route('password.request') }}">
        {{ csrf_field() }}
    </login>
@endsection
