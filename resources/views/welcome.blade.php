@extends('layouts.app')

@section('title', 'TravelConstruct')

@section('content')
        <section class="home-main-banner">
            <div class="img-bg"></div>
            <div class="text container">
                <h2>Best Software for travel agencies</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <div class="cta-wrap">
                    <a class="cta -white -bg-deep-orange" href="{{ url('/contact-us') }}">GET IN TOUCH</a>
                </div>
            </div>
        </section>
        <section id="welcome" class="container">
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class="flex justify-center">
                <div class="card card-medium margin-20-h">
                    <h2 class="card-head text-center"><a class="-orange" href="{{ url('/about-us') }}">About Us</a></h2>
                </div>
                <div class="card card-medium margin-20-h">
                    <h2 class="card-head text-center"><a class="-teal" href="{{ url('pricing') }}">Pricing</a></h2>
                </div>
                <div class="card card-medium margin-20-h">
                    <h2 class="card-head text-center"><a class="-purple" href="{{ url('contact-us') }}">Contact Us</a></h2>
                </div>
            </div>
        </section>
@endsection