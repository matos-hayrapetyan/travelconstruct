import Auth from './auth.js';
import Login from './login.vue';

window.Vue = require('vue');
window.VueResource = require('vue-resource');

Vue.use(VueResource);
Vue.use(Auth);

const app = new Vue({
    el: '#app',
    components: {
        Login,
    }
});
