import Modal from './Modal.vue';
import NewHotelModal from './NewHotelModal.vue';
import Auth from '../auth.js';


window.Vue = require('vue');

window.VueResource = require('vue-resource');

Vue.use(VueResource);
Vue.use(Auth);

new Vue({
    el: '#app',
    data: {
        showNewHotelModal: false,

    },
    components:{
        'modal':Modal,
        'NewHotelModal': NewHotelModal
    },


});